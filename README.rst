
.. image:: https://travis-ci.org/ZGCDDoo/statsfiles.svg?branch=master
   :alt: Build status of statsfiles on Travis CI
   :target: https://travis-ci.org/ZGDDoo/statsfiles
   
.. image:: https://codecov.io/gh/ZGCDDoo/statsfiles/branch/master/graph/badge.svg
  :target: https://codecov.io/gh/ZGCDDoo/statsfiles

You install like this (once in the parent directory):

pip install -e .

You run it like this:

python -m statsfiles iter_start --afm

wherre iter_start is a int and --afm is optional


Make sure your default python is python3.
